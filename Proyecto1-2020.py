
# encoding: utf-8
import os
from random import randint 
from time import sleep
matriz = []

#crear matriz solo con espacios
def crearMatriz(matriz):
    fila = [' ', ' ', ' ', ' ', ' ',' ', ' ', ' ', ' ', ' ',' ', ' ', ' ', ' ', ' ',' ', ' ', ' ', ' ', ' ']
    for i in range(0, 20):
        matriz.append(fila + [])
    return matriz

#dibujar matriz en pantalla
def imprimirMatriz(matriz):
  
    print("   ", end=" ")
    for i in range(0, 10):
        print(" ", end=" ")

    for i in range(10, 20):
        print(1,end=" ")

    print()
    print("   ",end=" ")
    for i in range(0, 10):
        print(i, end=" ")

    for i in range(0, 10):
        print(i,end=" ")
    print()
    i = 0
    for x in matriz:
        if(i < 10):
            print(" "+str(i) + "|",end=" ")
        else:
            print(str(i) + "|",end=" ")
        for y in x:
            print(y,end=" ")
        i += 1
        print("|")


#genera un camino aleatorio tomando números al azar
#entre 1 y 4 (incluyendo ambos) los cuales representan
#las orientaciones en el sentido de las agujas del reloj
def generarCamino(matriz):

    #posicion inicial
    posX = 0
    posY = 0
    while True:
        numero = randint(1, 4) #numero aleatorio

        #arriba
        if(numero == 1):
            nuevaPosX = posX - 1
            if(nuevaPosX >= 0): #comprueba si no se sale de la matriz
                posX = nuevaPosX
                matriz[posX][posY] = '*'

        #derecha
        elif(numero == 2):
            nuevaPosY = posY + 1
            if(nuevaPosY < 20): #comprueba si no se sale de la matriz
                posY = nuevaPosY
                matriz[posX][posY] = '*'

        #abajo
        elif(numero == 3):
            nuevaPosX = posX + 1
            if(nuevaPosX < 20): #comprueba si no se sale de la matriz
                posX = nuevaPosX
                matriz[posX][posY] = '*'

        #izquierda
        else:
            nuevaPosY = posY - 1
            if(nuevaPosY >= 0): #comprueba si no se sale de la matriz
                posY = nuevaPosY
                matriz[posX][posY] = '*'

        #cuando llega a la posicion final
        if(posX == 19 and posY == 19):
          break

    return matriz
    

def movimiento(posPersona, matriz, x, y):

    posX = posPersona[0]
    posY = posPersona[1]
    matriz[posX][posY] = "*"
    posPersona[x] = posPersona[x] + y
    posX = posPersona[0]
    posY = posPersona[1]
    matriz[posX][posY] = "X"


def iniciarJuego(matriz, posPersona):

    #ciclo del juego
    while True:
        imprimirMatriz(matriz)
        tecla = input("ingrese una tecla: ".upper())

        #hacia arriba
        if(tecla.upper() == "W"):
            posX = posPersona[0] - 1
            posY = posPersona[1]
            if(posX < 0 or matriz[posX][posY] == " "):#comprueba si no se sale de la matriz
                print("\n\tte saliste del camino\n".upper())
                sleep(1)
            else:
                movimiento(posPersona, matriz, 0, -1)

        #hacia la izquierda
        elif(tecla.upper() == "A"):
            posX = posPersona[0]
            posY = posPersona[1] - 1
            if(posY < 0 or matriz[posX][posY] == " "):#comprueba si no se sale de la matriz
                print("\n\tte saliste del camino\n".upper())
                sleep(1)
            else:
                movimiento(posPersona, matriz, 1, -1)

        #hacia abajo
        elif(tecla.upper() == "S"):
            posX = posPersona[0] + 1
            posY = posPersona[1]
            if(posX > 19 or matriz[posX][posY] == " "):#comprueba si no se sale de la matriz
                print("\n\tte saliste del camino\n".upper())
                sleep(1)
            else:
                movimiento(posPersona, matriz, 0, 1)


        #hacia la derecha
        elif(tecla.upper() == "D"):
            posX = posPersona[0]
            posY = posPersona[1] + 1
            if(posY > 19 or matriz[posX][posY] == " "):#comprueba si no se sale de la matriz
                print("\n\tte saliste del camino\n".upper())
                sleep(1)
            else:
                movimiento(posPersona, matriz, 1, 1)

        else:
            print("\n\tIngrese una tecla valida\n A= izquierda\n W=arriba \n D=derecha \n S=abajo\t" .upper())
            sleep(2)

        #comprueba si llega a la meta
        posX = posPersona[0]
        posY = posPersona[1]
        if(posX == 19 and posY == 19):
            print("\n\tGANASTE!!!!\n")
            opcion = input("\n\t¿QUIERES SEGUIR JUGANDO? (S/N): \n")
            seguir = False
            if(opcion.upper() == "S"):
                seguir = True
                break
            elif(opcion.upper() == "N"):
                print("ADIOS, VUELVA PRONTO!")
                seguir = False
                break
            else:
                print("\n\tIngrese una tecla valida\t".upper())
                sleep(1)

        os.system("clear")

    #inicia un juego nuevo si el usuario lo quiere
    if(seguir):
        matrizNueva = crearMatriz([])
        matrizNueva = generarCamino(matrizNueva)
        matrizNueva[0][0] = "X"
        iniciarJuego(matrizNueva, [0, 0])


#inicia el juego
matriz = crearMatriz(matriz)
matriz = generarCamino(matriz)
matriz[0][0] = "X"
iniciarJuego(matriz, [0, 0])
