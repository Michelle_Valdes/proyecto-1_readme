# Proyecto 1-Laberinto 🚀
En el siguiente proyecto de programación se presenta un juego de laberinto, en el cual se tiene que avanzar por un camino, el cual será generado de forma aleatoria al iniciar tu juego. Una vez se llegue al final, el jugador ganará y finalizará el juego! Tras esto, se le preguntará si quieres seguir jugando o simplemente quieres salir del juego.

## Comenzando 📋
Para poder jugar, se deben usar 4 teclas específicas, las cuales permitirán el movimiento dentro del juego. Estas son "A" (izquierda), "S" (abajo), "D" (derecha) y "W" (arriba). Por otro lado, si se llega a salir del cuadro de juego, se avisará en pantalla que no puedes avanzar de esa forma, mediante un mensaje de "Te saliste", y una vez que se llegue al final del juego, se imprimirá en pantalla un mensaje de "Ganaste"

## Pre-requisitos 📄
Para poder jugarse de forma correcta, se debe instalar en el quipo GNU/linux y tener instalado Python en su versión 3 o superior.
Para hacer esto se puede entrar al siguiente [link](https://www.python.org/downloads/)

## Pep8🔧⚙️
En este código utilizamos características de Pep8, tales como usar 4 espacios para indetación (Ejemplo: En funciones y ciclos), alineación de corchetes/paréntesis/llaves que cierran asignaciones, líneas menores a 79 carácteres y utiliación de líneas en blanco para separar partes diferentes del código

## Ejecutando el juego⌨️
Una vez se ejecute el juego, en pantalla se verá un cuadro de 20x20, donde se formará un pequeño camino/laberinto. Es en este, en el cual se debe mover nuestro personaje principal (X´man), el cual debe moverse por el camino delimitado hasta llegar al final, ganando así el juego.

## Construido con 🛠️
El juego fue desarrollado en base al lenguaje de programación Python en conjunto con el IDE PyCharm

- [Python]: Lenguaje de programación usado
- [PyCharm]: IDE utilizado

## Librerías usadas📖
- **os**
- **random** 
- **time**

## Comentarios📢
Para la realización del presente juego/proyecto, fue utilizado el lenguaje de programación Python en su versión 3.

## Autores ✒️🤓
- **Michelle Valdés Méndez**
- **Iván Olguin Cornejo**


*Estudiantes de primer año de Ingeniería Civil en Bioinformática - Universidad de Talca*





